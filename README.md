Amoeba Game w/ TDD
==================

Description
-----------

Create a simple multiplayer "amoeba" browser game. When you get to the page you will see the game's main menu, where you
 can set the two players' names and start a new game. If you start a game you will get a simple table as a playing board.
 The winner is the one who can set 5 equal marks in one row, column or diagonal. If the game has ended you get a simple
 alert message which tells who the winner is. During the whole game you are allowed to click on the "new game" or the
 "restart game" buttons. If you click on the new game button you get the main page with the deafult player names again
 (player1, player2). If you click on the restart game button the current game board and the score will be set to empty 
 and zero, and the game restarts.
 
###Basic game rules
- No undo
- You can set your mark only in empty cells, nothing happens if you click on an existing mark The winner is the one
  who can set 5 equal marks in one row, column or diagonal
- If the table is full the winner is the one who has the higher score
- dimensions of the board: 32 x 18

###You must
- implement a basic validation for the player names
    - Adding a player name is optional (by default Player1, Player2)
    - Custom name must be at least 1 character and maximum 8 characters long (trim, escape, prevent XSS )
        - if you click on the name it must be editable
        - if you hit enter the new name will be set
        - if you hit ESC it should cancel the "add name" process and set back the Player1 or Player2 default names
- count the scores in the actual game
    - every valid step adds extra 10 points to the user
    - if you set you mark next to one of your existing one it counts 20 points the
    - third mark next to the previous ones counts 30 points and so on...
    
###You must not
- save the scores (it can be deleted when the game has ended, no need to care about "high scores")
- save the player names (when you start a new game they can be set back to Player1 and Player2
- no time limit



Installation Instructions
-------------------------

First install the packages

    npm install

Run all commands (jshint -> jscs -> test + coverage -> jsdoc)

    npm run all

Run tests (also creates a bundle before testing):

    npm test

Run test coverage report (also creates a bundle before testing):

    npm run coverage

Run jsdoc generation:

    npm run jsdoc

Run jshint:

    npm run jshint

Run jscs (code style validation):

    npm run jscs
    
(Optional) Create bundle to be used with the *`tests/index.html`*:

    npm run bundle
