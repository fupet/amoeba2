Amoeba Game w/ TDD
==================

- When you start a game you will get a simple table
- The board's dimensions are 32x18
- **You can click on a cell and will set the mark**
- You can only set your mark on empty cells
- That player wins who has 5 marks next to each other in a row column or diagonal angle
