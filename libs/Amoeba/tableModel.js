var Backbone = require('backbone'),
	_ = require('underscore');

module.exports = Backbone.Model.extend(
	/** @lends TableModel.prototype */
	{
		/**
		 * Contains the game board, stores the game status.
		 * @private
		 * @type {Array}
		 */
		_table : [],

		/**
		 * This is the model for the game board.
		 *
		 * @augments external:Backbone.Model
		 * @param {Number} columns   The columns of the game board.
		 * @param {Number} rows      The rows of the game board.
		 * @constructs
		 */
		initialize : function(columns, rows) {
			module.exports.__super__.initialize.apply(this, arguments);
			this._columns = columns;
			this._rows = rows;
			this._table = Array(columns * rows);
		},

		/**
		 * Row number getter.
		 * @returns {Number}   The number of rows.
		 */
		getRows : function() {
			return this._rows;
		},

		/**
		 * Column number getter.
		 * @returns {Number}   The number of columns.
		 */
		getColumns : function() {
			return this._columns;
		},

		/**
		 * Set's a mark at the coordinates given.
		 * @param {Coordinate} coordinates   The location to put mark.
		 * @returns {void}
		 */
		addMarkAtPosition : function(coordinates) {
			this._table[this._calculateIndexFromCoordinates(coordinates)] = true;
			this.trigger('mark_added', this._getMaxMarkNext(coordinates));
		},

		_getMaxMarkNext : function(coordinates) {
			return Math.max(
				this._sameInARowFromPosition(coordinates),
				this._sameInAColumnFromPosition(coordinates),
				this._sameFromTopLeftToBottomRight(coordinates),
				this._sameFromTopRightToBottomLeft(coordinates)
			);
		},

		/**
		 * Calculates the index in the array for the given coordinates considering the number of columns.
		 * @param {Coordinate} coordinates   The coordinates to calculate from.
		 * @returns {Number}   The index in the array.
		 * @private
		 */
		_calculateIndexFromCoordinates : function(coordinates) {
			return this._columns * coordinates.y + coordinates.x;
		},

		/**
		 * Get's the mark from the table at the coordinate.
		 * @param {Coordinate} coordinates
		 * @returns {*}   The value at the mark.
		 * @private
		 */
		_getMarkAtCoordinates : function(coordinates) {
			return this._table[this._calculateIndexFromCoordinates(coordinates)];
		},

		/**
		 * Checks if the mark matches at the coordinate.
		 * @param {Coordinate} coordinates   The coordinate too look up.
		 * @param {*} mark   The mark to check against.
		 * @returns {boolean}   TRUE if the marks are the same.
		 * @private
		 */
		_isMarkAtCoordinateMatches : function(coordinates, mark) {
			return this._getMarkAtCoordinates(coordinates) === mark;
		},

		/**
		 * Finds the matching marks in a defined angle. Vertical or horizontal.
		 * @param startCoordinate
		 * @param dimension
		 * @param max
		 * @param zeroMark
		 * @returns {number}
		 * @private
		 */
		_addSameInALine : function(startCoordinate, dimension, max, zeroMark) {
			var stepCount = 0,
				currentCoordinates = _.clone(startCoordinate);

			--currentCoordinates[dimension];
			while (currentCoordinates[dimension] >= 0 &&
			this._isMarkAtCoordinateMatches(currentCoordinates, zeroMark)) {
				--currentCoordinates[dimension];
				++stepCount;
			}

			currentCoordinates = _.clone(startCoordinate);
			++currentCoordinates[dimension];
			while (currentCoordinates[dimension] < max &&
			this._isMarkAtCoordinateMatches(currentCoordinates, zeroMark)) {
				++currentCoordinates[dimension];
				++stepCount;
			}

			return stepCount;
		},

		/**
		 * Find the same marks in a row from the current position.
		 * @param {Coordinate} coordinates
		 * @returns {number}
		 * @private
		 */
		_sameInARowFromPosition : function(coordinates) {
			var positionZeroMark = this._getMarkAtCoordinates(coordinates),
				stepCount = 1;

			stepCount += this._addSameInALine(coordinates, 'x', this._columns, positionZeroMark);

			return stepCount;
		},

		/**
		 * Find the same marks in a column from the current position.
		 * @param {Coordinate} coordinates
		 * @returns {number}
		 * @private
		 */
		_sameInAColumnFromPosition : function(coordinates) {
			var positionZeroMark = this._getMarkAtCoordinates(coordinates),
				stepCount = 1;

			stepCount += this._addSameInALine(coordinates, 'y', this._rows, positionZeroMark);

			return stepCount;
		},

		/**
		 * Find the same marks in a diagonal angle to left from the current position.
		 * @param {Coordinate} coordinates
		 * @returns {number}
		 * @private
		 */
		_sameFromTopLeftToBottomRight : function(coordinates) {
			var positionZeroMark = this._getMarkAtCoordinates(coordinates),
				stepCount = 1,
				currentCoordinates = _.clone(coordinates);

			--currentCoordinates.x;
			--currentCoordinates.y;
			while (currentCoordinates.x >= 0 && currentCoordinates.y >= 0 &&
			this._isMarkAtCoordinateMatches(currentCoordinates, positionZeroMark)) {
				--currentCoordinates.x;
				--currentCoordinates.y;
				++stepCount;
			}

			currentCoordinates = _.clone(coordinates);
			++currentCoordinates.x;
			++currentCoordinates.y;
			while (currentCoordinates.x < this._columns && currentCoordinates.y < this._rows &&
			this._isMarkAtCoordinateMatches(currentCoordinates, positionZeroMark)) {
				++currentCoordinates.x;
				++currentCoordinates.y;
				++stepCount;
			}

			return stepCount;
		},

		/**
		 * Find the same marks in a diagonal angle to right from the current position.
		 * @param {Coordinate} coordinates
		 * @returns {number}
		 * @private
		 */
		_sameFromTopRightToBottomLeft : function(coordinates) {
			var positionZeroMark = this._getMarkAtCoordinates(coordinates),
				stepCount = 1,
				currentCoordinates = _.clone(coordinates);

			++currentCoordinates.x;
			--currentCoordinates.y;
			while (currentCoordinates.y >= 0 && currentCoordinates.x < this._columns &&
			this._isMarkAtCoordinateMatches(currentCoordinates, positionZeroMark)) {
				++currentCoordinates.x;
				--currentCoordinates.y;
				++stepCount;
			}

			currentCoordinates = _.clone(coordinates);
			--currentCoordinates.x;
			++currentCoordinates.y;
			while (currentCoordinates.y < this._rows && currentCoordinates.x >= 0 &&
			this._isMarkAtCoordinateMatches(currentCoordinates, positionZeroMark)) {
				--currentCoordinates.x;
				++currentCoordinates.y;
				++stepCount;
			}

			return stepCount;
		}
	});
