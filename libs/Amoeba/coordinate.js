/**
 * This class stores the coordinates.
 * @name Coordinate
 * @param {Number} x   The horizontal coordinate.
 * @param {Number} y   The vertical coordinate.
 * @constructs
 */
module.exports = function(x, y) {
	this.x = x;
	this.y = y;
};
