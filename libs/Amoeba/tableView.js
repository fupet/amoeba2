var Backbone = require('backbone');

module.exports = Backbone.View.extend(
	/** @lends TableView.prototype */
	{
		winnerMarkCount : 5,
		/**
		 * This is a view for the table.
		 *
		 * @augments external:Backbone.View
		 * @constructs
		 */
		initialize : function() {
			module.exports.__super__.initialize.apply(this, arguments);
		},

		/**
		 * Renders the game table.
		 * @returns {TableView}
		 */
		render : function() {
			module.exports.__super__.render.apply(this, arguments);
			var rows = this.model.getRows(),
				cols = this.model.getColumns(),
				itemCount = rows * cols,
				cells = '';

			for (var i = 0; i < itemCount; ++i) {
				cells += '<div data-x="' + (i % cols) + '" data-y="' + Math.floor(i / rows) + '"/>';
			}

			$(cells).appendTo(this.$el);

			this.$el.on('click', 'div', $.proxy(this.onTableCellClick, this));
			return this;
		},

		/**
		 * Event handler for the cell click delegation.
		 * @param {jQuery.Event} ev   The click event object.
		 */
		onTableCellClick : function(ev) {
			var $target = $(ev.target);
			this.model.addMarkAtPosition($target.data('x'), $target.data('y'));
		},

		onMarkAdded : function(number) {
			if (this.winnerMarkCount <= number) {
				this.trigger('winner');
			}
		}
	});