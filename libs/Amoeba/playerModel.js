var Backbone = require('backbone');

module.exports = Backbone.Model.extend(
/** @lends PlayerModel.prototype */
{
	/**
	 * The player's name.
	 * @private
	 * @type {String}
	 */
	_name : null,

	/**
	 * This is a model for the player.
	 * @augments external:Backbone.Model
	 * @param {String} name   The player's name.
	 * @constructs
	 */
	initialize : function(name) {
		module.exports.__super__.initialize.apply(this, arguments);
		this._name = name;
	}
});