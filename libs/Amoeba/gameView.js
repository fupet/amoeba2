var Backbone = require('backbone'),
	TableModel = require('./tableModel');
	PlayerModel = require('./playerModel');

module.exports = Backbone.View.extend(
	/** @lends GameView.prototype */
	{
		id : 'game',

		ui : {
			startGameButton : '#startGame'
		},

		events: {
			'click #startGame' : 'onStartNewGameButtonClicked'
		},

		_players : [],

		/**
		 * This a view for the game.
		 * @augments external:Backbone.View
		 * @constructs
		 */
		initialize : function() {
			module.exports.__super__.initialize.apply(this, arguments);
		},

		/**
		 * Event listener for the start new game button click.
		 * @return {void}
		 */
		onStartNewGameButtonClicked : function() {
			this._startNewGame();
		},

		/**
		 * Handles the new game initialization.
		 * @private
		 * @return {void}
		 */
		_startNewGame : function() {
			this._tableModel = new TableModel(32, 18);
			this._players.push(new PlayerModel('Player1'));
		}
	});
