var assert = require('chai').assert,
	sinon = require('sinon'),
	_ = require('underscore'),
	TableModel = require('../../libs/Amoeba/tableModel');

suite('test the table model', function() {
	var tableSizeDataProvider = [
			[3, 3, 9],
			[10, 30, 300],
			[6, 5, 30],
			[32, 18, 576]
		],
		tableMarkedDataProvider = [
			[5, 3, 101],
			[15, 13, 431],
			[1, 17, 545]
		],
		getArrayWithDataSetAtPositions = function(positions, value) {
			var filledTable = [];
			positions.forEach(function(index) {
				filledTable[index] = value;
			});
			return filledTable;
		},
		countInARowDataProvider = [
			[
				getArrayWithDataSetAtPositions([360, 361, 362, 363, 364, 358], true),
				5,
				{x : 9, y : 11}
			],
			[
				getArrayWithDataSetAtPositions([500, 0, 50, 10, 250, 15, 185], true),
				1,
				{x : 25, y : 5}
			],
			[
				getArrayWithDataSetAtPositions([0, 1, 2, 3, 6, 7, 8, 9], true),
				4,
				{x : 2, y : 0}
			]
		],
		countInAColumnDataProvider = [
			[
				getArrayWithDataSetAtPositions([329, 361, 393, 425, 457, 358], true),
				5,
				{x : 9, y : 11}
			],
			[
				getArrayWithDataSetAtPositions([500, 0, 50, 10, 250, 15, 185], true),
				1,
				{x : 25, y : 5}
			],
			[
				getArrayWithDataSetAtPositions([0, 32, 64, 96, 256, 160, 224, 9], true),
				4,
				{x : 0, y : 2}
			]
		],
		countInALeftAngleDataProvider = [
			[
				getArrayWithDataSetAtPositions([328, 361, 394, 427, 460, 358], true),
				5,
				{x : 9, y : 11}
			],
			[
				getArrayWithDataSetAtPositions([500, 0, 50, 10, 250, 15, 185], true),
				1,
				{x : 25, y : 5}
			],
			[
				getArrayWithDataSetAtPositions([0, 33, 66, 99, 256, 160, 224, 9], true),
				4,
				{x : 1, y : 1}
			]
		],
		countInARightAngleDataProvider = [
			[
				getArrayWithDataSetAtPositions([330, 361, 392, 423, 454, 358], true),
				5,
				{x : 9, y : 11}
			],
			[
				getArrayWithDataSetAtPositions([500, 0, 50, 10, 250, 15, 185], true),
				1,
				{x : 25, y : 5}
			],
			[
				getArrayWithDataSetAtPositions([10, 41, 72, 103, 256, 160, 224, 9], true),
				4,
				{x : 10, y : 0}
			]
		];
	setup(function() {

	});

	tableSizeDataProvider.forEach(function(tableSizeData) {
		test('it should have an array that has ' + tableSizeData[0] + 'x' + tableSizeData[1] + ' length', function() {
			var sampleTable = Array(tableSizeData[2]),
				tableModel = new TableModel(tableSizeData[0], tableSizeData[1]);

			assert.deepEqual(tableModel._table, sampleTable, 'The table is not created properly.');
		});

		test('test the number of cells', function() {
			var tableModel = new TableModel(tableSizeData[0], tableSizeData[1]);
			assert.lengthOf(tableModel._table, tableSizeData[2], 'The given cell number is not correct.')
		});
	}, this);

	tableMarkedDataProvider.forEach(function(tableData) {
		test('is position marked: ' + tableData[0] + ',' + tableData[1], function() {
			var tableModel = new TableModel(32, 18);
			tableModel.addMarkAtPosition({x : tableData[0], y : tableData[1]});
			assert.isTrue(tableModel._table[tableData[2]], 'The mark is not set on position.');
		});
	});

	countInARowDataProvider.forEach(function(rowTest) {
		test('count marks in a row from position: ' + rowTest[2].x + ',' + rowTest[2].y, function() {
			var tableModel = new TableModel(32, 18);
			_.extend(tableModel._table, rowTest[0]);
			assert.strictEqual(tableModel._sameInARowFromPosition(rowTest[2]), rowTest[1], 'Same in a row count failed.');
		});
	});

	countInAColumnDataProvider.forEach(function(colTest) {
		test('count marks in a column from position: ' + colTest[2].x + ',' + colTest[2].y, function() {
			var tableModel = new TableModel(32, 18);
			_.extend(tableModel._table, colTest[0]);
			assert.strictEqual(tableModel._sameInAColumnFromPosition(colTest[2]), colTest[1], 'Same in a column count failed.');
		});
	});

	countInALeftAngleDataProvider.forEach(function(diagonalTest) {
		test('count marks in a left angle from position: ' + diagonalTest[2].x + ',' + diagonalTest[2].y, function() {
			var tableModel = new TableModel(32, 18);
			_.extend(tableModel._table, diagonalTest[0]);
			assert.strictEqual(tableModel._sameFromTopLeftToBottomRight(diagonalTest[2]), diagonalTest[1], 'Same in left angle count failed.');
		});
	});

	countInARightAngleDataProvider.forEach(function(diagonalTest) {
		test('count marks in a right angle from position: ' + diagonalTest[2].x + ',' + diagonalTest[2].y, function() {
			var tableModel = new TableModel(32, 18);
			_.extend(tableModel._table, diagonalTest[0]);
			assert.strictEqual(tableModel._sameFromTopRightToBottomLeft(diagonalTest[2]), diagonalTest[1], 'Same in left angle count failed.');
		});
	});

	test('get mark place success', function() {
		var tableModel = new TableModel(32, 18),
			handlerSpy = sinon.spy();
		tableModel.on('mark_added', handlerSpy);
		_.extend(tableModel._table, countInARowDataProvider[0][0]);
		tableModel.addMarkAtPosition(countInARowDataProvider[0][2]);

		assert.isTrue(handlerSpy.calledOnce, 'The mark set wasn\'t success.');
		assert.isTrue(handlerSpy.calledWith(countInARowDataProvider[0][1]), 'The returned mark count not correct.');
	});

	teardown(function() {
	});
});
