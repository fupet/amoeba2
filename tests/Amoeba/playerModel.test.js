var assert = require('chai').assert,
	sinon = require('sinon'),
	PlayerModel = require('../../libs/Amoeba/playerModel');

suite('test the gameboard', function() {
	setup(function() {
		this.playerModel = new PlayerModel('player1');
	});

	test('player model created with name player1', function() {
		assert.equal(this.playerModel._name, 'player1');
	});

	teardown(function() {
	});
});
