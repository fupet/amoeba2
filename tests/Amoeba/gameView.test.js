var assert = require('chai').assert,
	sinon = require('sinon'),
	GameView = require('../../libs/Amoeba/gameView'),
	testContent = require('./content/testcontent.html'),
	testUtils = require('../setup_utils');

suite('test game start', function() {
	setup(function() {
		testUtils.loadTestContent(testContent);

		this.sinonSandbox = sinon.sandbox.create();
		this.sinonSandbox.spy(GameView.prototype, 'onStartNewGameButtonClicked');

		this.gameView = new GameView({
			el : '#game'
		}).render();

		this.startGameButton = this.gameView.$(this.gameView.ui.startGameButton);
	});

	test('if the start new game event called', function() {
		this.startGameButton.click();
		assert.isTrue(this.gameView.onStartNewGameButtonClicked.calledOnce, 'The game start event callback is not called.');
	});

	test('if the table model created on the view on game start', function() {
		var mockTableModel = {_table : Array(568)};
		this.startGameButton.click();
		assert.notDeepEqual(this.gameView._tableModel, mockTableModel, 'The table model is not created.');
	});

	test('if player1 is inited', function() {
		var mockPlayerModel = {
			name : 'Player1'
		};
		this.startGameButton.click();
		assert.equal(this.gameView._players[0]._name, 'Player1', 'The player1 is not created.');
	});

	teardown(function() {
		this.sinonSandbox.restore();
		this.gameView.remove();
	});
});
