var assert = require('chai').assert,
	sinon = require('sinon'),
	TableView = require('../../libs/Amoeba/tableView'),
	testContent = require('./content/testcontent.html'),
	testUtils = require('../setup_utils');

suite('table view test', function() {
	setup(function() {
		testUtils.loadTestContent(testContent);

		this.sinonSandbox = sinon.sandbox.create();
		this.sinonSandbox.spy(TableView.prototype, 'onTableCellClick');
		this.tableView = new TableView({
			el : '#table'
		});
	});

	var mockModelWithDimensions = function(cols, rows) {
			return {
				_table            : Array(cols * rows),
				getNumberOfCells  : function() {
					return cols * rows;
				},
				addMarkAtPosition : function() {

				},
				getRows           : function() {
					return rows;
				},
				getColumns        : function() {
					return cols;
				}
			};
		},
		cellCreationDataProvider = [
			[3, 3, 9],
			[16, 6, 96],
			[32, 18, 576]
		],
		tableCellClickDataProvider = [
			[10, 3, 29],
			[32, 18, 2],
			[14, 10, 55]
		];

	cellCreationDataProvider.forEach(function(size) {
		test('cell creation on the dom from the model: ' + size, function() {
			this.tableView.model = mockModelWithDimensions.apply(mockModelWithDimensions, size);
			this.tableView.render();
			assert.lengthOf(this.tableView.$el.children(), size[2], 'Table cells are not created.');
		});
	});

	test('test event listener on cell', function() {
		this.tableView.model = mockModelWithDimensions(5, 2);
		this.tableView.render();
		this.tableView.$el.children().eq(2).click();
		this.tableView.$el.children().eq(3).click();
		assert.isTrue(this.tableView.onTableCellClick.calledTwice, 'Event callback is not called.');
	});

	tableCellClickDataProvider.forEach(function(cellClickData) {
		test('call model with table cell click with position: ' + cellClickData[2], function() {
			this.tableView.model = mockModelWithDimensions.apply(mockModelWithDimensions, cellClickData);
			this.sinonSandbox.spy(this.tableView.model, 'addMarkAtPosition');
			this.tableView.render();
			this.tableView.$el.children().eq(cellClickData[2]).click();
			assert.isTrue(
				this.tableView.model.addMarkAtPosition.calledWith(
					cellClickData[2] % cellClickData[0],
					Math.floor(cellClickData[2] / cellClickData[1])
				), 'The view not passed the proper value to the model.');
		});
	});

	test('winner click test', function() {
		this.tableView.model = mockModelWithDimensions(32, 18);
		var handlerSpy = sinon.spy();
		this.tableView.on('winner', handlerSpy);
		this.tableView.render();
		this.tableView.onMarkAdded(5);

		assert.isTrue(handlerSpy.calledOnce, 'Winner event not triggered.');

	});

	teardown(function() {
		this.sinonSandbox.restore();
		this.tableView.remove();
	});
});
